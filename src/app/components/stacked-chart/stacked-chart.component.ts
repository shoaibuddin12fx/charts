import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Chart, ChartDataset, registerables } from 'chart.js';
import { ChartService } from 'src/app/services/chart.service';

@Component({
  selector: 'app-stacked-chart',
  templateUrl: './stacked-chart.component.html',
  styleUrls: ['./stacked-chart.component.scss']
})
export class StackedChartComponent implements OnInit {

  @ViewChild('imageCanvas') canvas!: ElementRef;
  private ctx!: CanvasRenderingContext2D;
  barChart: any;

  constructor(
    public chartService: ChartService
  ) { }

  ngOnInit(): void {
  }

  async ngAfterViewInit() {
    Chart.register(...registerables);
    setTimeout(() => {
      this.barChartMethod();
    }, 2000);
  }

  async barChartMethod() {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    let datasets: ChartDataset[] = [];
    let lables: unknown[] = [];

    // Call the Service

    datasets = await this.chartService.getStackedData();
    lables = this.chartService.stacked_labels;

    let obj = {
      x: {
        stacked: true,
      },
      y: {
        stacked: true
      }
    }

    const text = "Trigonometry, Probability, Algebra, Arithematics";

    //Styling and rendering the Chart Using Chart.js

    this.barChart = new Chart(this.ctx, {
      type: "bar",
      data: {
        labels: lables,
        datasets: datasets
      },
      options: {
        responsive: true,
        indexAxis: "x",
        scales: obj,
        plugins: {
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: text,
            font: {
              size: 23
            }
          }
        }
      }
    });
  }

}
