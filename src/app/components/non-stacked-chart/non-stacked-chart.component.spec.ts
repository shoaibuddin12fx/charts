import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NonStackedChartComponent } from './non-stacked-chart.component';

describe('NonStackedChartComponent', () => {
  let component: NonStackedChartComponent;
  let fixture: ComponentFixture<NonStackedChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NonStackedChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NonStackedChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
