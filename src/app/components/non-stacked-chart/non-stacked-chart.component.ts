import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Chart, ChartDataset, registerables } from 'chart.js';
import { ChartService } from 'src/app/services/chart.service';

@Component({
  selector: 'app-non-stacked-chart',
  templateUrl: './non-stacked-chart.component.html',
  styleUrls: ['./non-stacked-chart.component.scss']
})
export class NonStackedChartComponent implements OnInit {

  @ViewChild('imageCanvas') canvas!: ElementRef;
  private ctx!: CanvasRenderingContext2D;
  barChart: any;

  constructor(
    public chartService: ChartService
  ) { }


  ngOnInit(): void {
  }

  async ngAfterViewInit() {
    Chart.register(...registerables);
    setTimeout(() => {
      this.barChartMethod();
    }, 2000);
  }

  async barChartMethod() {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    let datasets: ChartDataset[] = [];
    let lables: unknown[] = [];

    // Call the Service

    datasets = await this.chartService.getNonStackedData();
    lables = await this.chartService.getNonStacklables();

    const text = "By Count";

    //Styling and rendering the Chart Using Chart.js
    this.barChart = new Chart(this.ctx, {
      type: "bar",
      data: {
        labels: lables,
        datasets: datasets
      },
      options: {
        responsive: true,
        indexAxis: "y",
        scales: {},
        plugins: {
          legend: {
            position: 'left',
            align: "center",
            labels: {
              font: {
                size: 14
              }
            }
          },
          title: {
            display: true,
            position: 'left',
            text: text,
            align: "center",
            font: {
              size: 18
            }
          }
        }
      }
    });
  }

}
