import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ChartComponent } from './components/chart/chart.component';
import { StackedChartComponent } from './components/stacked-chart/stacked-chart.component';
import { NonStackedChartComponent } from './components/non-stacked-chart/non-stacked-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    ChartComponent,
    StackedChartComponent,
    NonStackedChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
