import { Injectable } from '@angular/core';
import { ChartDataset } from 'chart.js';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  non_stacked_data = require('src/assets/non_stacked.json');
  stacked_data = require('src/assets/stacked.json');
  stacked_labels: any[] = ["Trigonometry", "Probability", "Algebra", "Arithematics"];

  constructor() { }

  // Get a list of random colors

  generateRandomColors(num: number) {
    let colors = [];
    for (let index = 0; index < num; index++) {
      let randomColor = Math.floor(Math.random() * 16777215).toString(16);
      colors.push("#" + randomColor);
    }
    return colors;
  }

  // get the dataset to prepare for non stacked chart

  getNonStackedData() {
    return new Promise<ChartDataset[]>(async resolve => {
      let datasets: any[] = [];

      const keys = Object.keys(this.non_stacked_data[0]);
      keys.splice(0, 1);
      let labels = [
        {
          key: "total_attempts",
          value: "Total Attempts",
          data: [0]
        },
        {
          key: "correct_attempts",
          value: "Correct Attempts",
          data: [0]
        },
        {
          key: "wrong_attempts",
          value: "Wrong Attempts",
          data: [0]
        },
        {
          key: "not_attempts",
          value: "Not Attempted",
          data: [0]
        }
      ];


      // Running filters for each individual label for each Question
      for (let index = 0; index < keys.length; index++) {
        const element = keys[index];

        let total: any[] = this.non_stacked_data.filter((x: { [x: string]: number; }) => {
          return x[element] == 1 || x[element] == 0
        }).map((x: { [x: string]: any; }) => x[keys[index]]);
        labels[0].data.push(total.length);

        let correct: any[] = this.non_stacked_data.filter((x: { [x: string]: number; }) => {
          if (x[element] == 1) {
            return x;
          } else {
            return null;
          }
        }).map((x: { [x: string]: any; }) => x[keys[index]]);
        labels[1].data.push(correct.length);


        let wrong: any[] = this.non_stacked_data.filter((x: { [x: string]: number; }) => {
          if (x[element] == 0) {
            return x;
          } else {
            return null;
          }
        }).map((x: { [x: string]: any; }) => x[keys[index]]);
        labels[2].data.push(wrong.length);

        let not: any[] = this.non_stacked_data.filter((x: { [x: string]: string; }) => {
          if (x[element] == "-") {
            return x;
          } else {
            return null;
          }
        }).map(((x: { [x: string]: any; }) => x[keys[index]]));
        labels[3].data.push(not.length);
      }


      labels.forEach(element => {
        element.data.splice(0, 1);
        datasets.push({
          label: element.value,
          data: element.data,
          backgroundColor: this.generateRandomColors(1),
          options: {
            scales: {
              xAxes: [{
                stacked: true
              }],
              yAxes: [{
                stacked: true
              }]
            }
          }
        })
      });
      console.log(datasets);
      resolve(datasets);
    })
  }

  // get the chart labels for non stacked 
  getNonStacklables() {
    return new Promise<unknown[]>(async resolve => {
      const keys = Object.keys(this.non_stacked_data[0]);
      keys.splice(0, 1);
      resolve(keys);
    })
  }

  getStackedData() {
    return new Promise<ChartDataset[]>(async resolve => {
      let _labels = [
        {
          key: "correct",
          value: "Correct Questions",
          data: [0, 0, 0, 0]
        },
        {
          key: "incorrect",
          value: "Incorrect Questions",
          data: [0, 0, 0, 0]
        },
        {
          key: "non_attempted",
          value: "Non Attempted Questions",
          data: [0, 0, 0, 0]
        }
      ];

      // Running filters for each individual label for each key of _labels

      for (let index = 0; index < this.stacked_labels.length; index++) {
        const element = this.stacked_labels[index];
        let total: any[] = this.stacked_data.filter((x: { [x: string]: any; }) => {
          return x[element] == 1 || x[element + "__2"] == 1 || x[element + "__1"] == 1 || x[element + "__3"] == 1;
        });
        console.log(total);
        _labels[0].data[index] = total.length;
        console.log(_labels);

        let total_2: any[] = this.stacked_data.filter((x: { [x: string]: any; }) => {
          return x[element] == 0 || x[element + "__2"] == 0 || x[element + "__1"] == 0 || x[element + "__3"] == 0;
        });
        console.log(total_2);
        _labels[1].data[index] = total_2.length;
        console.log(_labels);

        let total_3: any[] = this.stacked_data.filter((x: { [x: string]: any; }) => {
          return x[element] == "-" || x[element + "__2"] == "-" || x[element + "__1"] == "-" || x[element + "__3"] == "-";
        });
        console.log(total_3);
        _labels[2].data[index] = total_3.length;
        console.log(_labels);
      }

      let labels = [
        {
          label: "Correct Questions",
          data: _labels[0].data,
          backgroundColor: this.generateRandomColors(1),
        },
        {
          label: "Incorrect Questions",
          data: _labels[1].data,
          backgroundColor: this.generateRandomColors(1),
        },
        {
          label: "Non Attempted Questions",
          data: _labels[2].data,
          backgroundColor: this.generateRandomColors(1),
        }
      ];
      resolve(labels);
    })
  }
}
